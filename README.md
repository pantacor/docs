# README

## Documentation Index Layout

The layout for this documentation is inspired by the great [DIVIO documentation system](https://documentation.divio.com/) and follows this order:

* Introduction: both technical and commercial
* Get Started: redirects readers to other sections depending on their objective
* How to Prepare your Device: RPi, other devices, app engine
* How to Manage your Device: Pantacor Hub, from host, from device
* How to Build: guidelines on how to build apps and Pantavisor BSP
* Technical Overview: to explain Pantavisor features from a top level point of view
* References: easy to consult in depth information
* Other Tutorials & Showcases
* Downloads

## How to test before landing

### How to run it with LiveReload

```bash
git clone --recurse-submodules git@gitlab.com:pantacor/docs.git
cd docs
docker-compose up docs
```

Go to http://localhost:8080/ to your livereload server

### How to locally test changes in this repo

To check how your changes in docs look like, you can use docker:

```bash
docker build -f Dockerfile -t docs .
docker run -p 8080:80 docs
```

A nginx instance is now running locally. In your web browser, go to http://localhost:8080/ to access the built docs.
