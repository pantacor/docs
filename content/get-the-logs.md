# Get the Logs

The device [stored logs](storage.md#logs) are pushed to Pantacor Hub by default and can be monitored from the [device dashboard](ph-device-dashboard.md) in the `Logs` tab:

![](images/ph-logs.png)
