# Get Started

In this guide, we will guide you through the process of flashing your `Raspberry Pi` device with a ready-to-use pre-compiled image.

!!! Note
    Before starting this guide, we recommend to [create a Pantacor Hub account](register-user.md) for new users. However, there are [different ways](choose-way.md) to manage your Pantavisor device, and a local experience that does not require registration is available.

## Requirements

To follow this guide, you will need the following:

- `Raspberry Pi 3 B+` or `Raspberry Pi 4`
- A micro SD Card
- Micro SD Card Writer

!!! Note
    If you are missing any of these requirements, you can have a look at [how to install other images](choose-image.md), incluiding emulated ones, or at [how to run Pantavisor as a deamon](requirements-appengine.md). Finally, as a last resource, you can also [build your own images](environment-setup.md). 

## Tutorial Overview

1. [Image setup](image-setup-rpi3.md): Download and flash our pre-compiled Pantavisor image on a micro SD card.
2. [First boot](first-boot-rpi3.md): Insert the micro SD card in the board slot and boot it up.

After this, you will be able to [manage your device](choose-way.md) both locally and remotely.
