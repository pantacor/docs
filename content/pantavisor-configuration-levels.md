# Pantavisor Configuration

!!! Note
    The configuration syntax is common for all levels, but not all levels support the same keys. Our [reference](pantavisor-configuration.md) contains the list of keys and the allowed levels for each one.

There are four ways to set Pantavisor configuration, depending on when it can be modified:

* [Compile time](#compile-time)
* [Boot up time](#boot-up-time)
* [Update](#update)
* [Runtime](#runtime)

Bear in mind that not all configuration parameters are available for all levels. Furthermore, each level will overwrite whatever is configured in the previous ones.

## Compile Time

The most complete way to tweak with Pantavisor configuration is to do it at [build](environment-setup.md) time. This is done in the `pantavisor.config` and `pantahub.config` files in the [source code](customize-build-pantavisor.md#config).

## Boot up time

There are three options available:

* [cmdline](#cmdline)
* [Environment Variables](#environment-variables)
* [Policies](#policies)

We also have an overriding mechanism inside of the boot up time configuration options, as env variables overrules cmdline and policies does the same with env variables.

### cmdline

!!! Warning
    This method is _DEPRECATED_ but still supported for backwards compatibility reasons. It is recommended to use [env variables](#environment-variables) instead.

Right after loading the [configuration files](#compile-time), Pantavisor reads `/proc/cmdline` in search for `key=value` pairs that use the prefix `ph_` or `pv_`. This can be done from the [bootloader console](navigating-console.md#bootloader-console).

### Environment Variables

Linux environment variables can be used to configure Pantavisor. To do that, the rules to set env variables have to be followed:

* Use `key=value` format
* Do not use `.`
* If ` ` characted is needed, you can escape them by using `"` between the config item. For example: `"PV_SYSCTL_KERNEL_CORE_PATTERN=|/lib/pv/pvcrash --skip"`.

These variables need to be set during boot time, and setting them after that will have no effect on Pantavisor. This can be achieved from the [bootloader console](navigating-console.md#bootloader-console).

### Policies

Policies are added at build time from the [vendor skel directory](customize-build-pantavisor.md#vendor), but loaded during boot up time.

To select a policy among the installed ones, we need to set its name to the `PV_POLICY` key using any of the previous configuration methods.

## Update

Some of the configuration keys can be overriden when updating a device to a new [revision](revisions.md). To do so, Pantavisor offers:

* [OEM](#oem)

The configuration changes will override the previous methods and will only affect the currently running revision.

### OEM

For setups where we want to modify the configuration based on device [updates](updates.md), we offer the possibility to fix a configuration file to a revision.

Its location [inside the revision](pantavisor-state-format-v2.md#oem-configuration) will be defined by the [configuration](pantavisor-configuration.md) values of the keys `PV_OEM_NAME` and `PV_POLICY` from previous levels.

## Runtime

A smaller subset of configuration keys can be directly set during runtime, using any of these two methods:

* [User Metadata](#user-metadata)
* [Commands](#commands)

As well as with the other levels, commands will overrule user metadata, and user metadata will override the values set in previous levels. Furthermore, using one or the other will also have an affect on the persistence of the configured values after a device reboot.

### User Metadata

!!! Note
    If the [user metadata volume](pantavisor-state-format-v2.md#devicejson) is assigned to a permanent volume, as it is by default, these changes will persist over device reboots.

[User metadata](storage.md#user-metadata) can be used to override any of the previously presented configuration mechanisms.

There is a number of ways of setting user metadata, depending on the device management method choice. Go to our [how-to use Pantavisor guide](choose-way.md) for more information.

### Commands

!!! Note
    It is important to notice that these changes will not persist after a device reboot in any case.

The [Pantavisor control socket](local-control.md#local-control), or consequently the [PVControl tool](local-control.md#pvcontrol), offers another way to change a very limited subset of configuration values. Specifically, using the [command](https://docs.pantahub.com/pantavisor-commands/#commands) endpoint.
