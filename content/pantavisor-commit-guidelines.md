# Pantavisor Commit Guidelines

In order to keep an homogeneous and encourage thoughtful commit messages, we follow the [Conventional Commits Specification](https://www.conventionalcommits.org/en/v1.0.0/) for our commit messages:

```
<type>: <description>

[optional body]

[optional footer(s)]
```

In the future, we will enforce this through our CI.
