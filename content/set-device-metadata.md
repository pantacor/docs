# Set User Metadata

User metadata can be used to send information from the cloud to the device. Check the [metadata reference](pantavisor-metadata.md#user-metadata) to see which values are parsed by default in Pantavisor.

To set metadata key/value pairs, just go to the tab `Configuration` in your [device dashboard](ph-device-dashboard.md).

![](images/user-meta.png)

You can also set user metadata at a global level for all your present and future devices in the `Global Configuration` tab on the left side of the UI.
