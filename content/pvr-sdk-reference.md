# pvr-sdk

Welcome to the [pvr-sdk container](https://gitlab.com/pantacor/pv-platforms/pvr-sdk) reference! In the following chapters, you will find the reference pages for some of our [Pantavisor local control](local-control.md) tools: [pantabox](pvr-sdk/reference/pantabox.md), [pvtx](pvr-sdk/pvtx-app/content/api-readme.md) and [pvcontrol](pvr-sdk/reference/pvcontrol.md).
