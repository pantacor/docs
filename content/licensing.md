# Licensing

Pantavisor is MIT licensed, which means tons of flexibility for commercial projects, without the headaches of a pure GPL codebase. The container runtime plugin system makes a clear separation of the provider library and Pantavisor itself.
