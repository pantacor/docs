# Where to Start?

This page will help you on deciding how to navigate this documentation.

## Getting Started

If you are about to embark on a new project or you just want to give a taste to [Pantavisor](pantavisor-architecture.md), you will first have to decide which [device or devices](choose-device.md) are you going to use, and which path are you going to choose for [managing](choose-way.md) them.

* [How to Prepare your Device](choose-device.md)
* [How to Manage your Device](choose-way.md)

## Building from Source Code

If you are an application developer, you have already [setup your devices](choose-device.md), and you can [manage](choose-way.md) them, then you are ready to move on to the next stage and focus on what is important to you: [creating the application level](create-apps.md).

If you want to make infrastructure changes in the [BSP](bsp.md), including Pantavisor, then you can follow by [building its source code](environment-setup.md).

* [How to Build Apps](create-apps.md)
* [How to Build Pantavisor](environment-setup.md)

## Understanding Pantavisor

For a deeper understanding on Pantavisor, you can first check the [technical overview](pantavisor-architecture.md), which can be read from top to bottom to understand all its features.

There are also [quick reference pages](build-options.md), meant to be easy to navigate and informative.

* [Technical Overview](pantavisor-architecture.md)
* [References](build-options.md)
