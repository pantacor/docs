const json2md = require("json2md")
const fs = require('fs');

var myArgs = process.argv.slice(2);

let raw = fs.readFileSync(myArgs[0]);
let images = JSON.parse(raw);

console.log(json2md(images))
