#!/usr/bin/python

import sys
import boto3
import json
import dateutil.parser
import pathlib

def authenticate(access_key, secret_key):
    return boto3.client('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_key)

def collect_pipelines_info(session, bucket, path):
    paginator = session.get_paginator('list_objects_v2')
    pages = paginator.paginate(Bucket=bucket, Prefix=path)
    print("collect_pipelines_info: get objects from ", bucket, "/", path, "...")

    pipelines = {}
    for page in pages:
        print("collect_pipelines_info:", len(page['Contents']), "objects retrieved from page")
        for obj in page['Contents']:
            obj_list = obj['Key'].split('/')
            if len(obj_list) > 4:
                pipe = pipelines.setdefault(int(obj_list[3]), {'architectures':{},'metadata':None})
                file_name = obj_list[4]
                if file_name == "pipeline.json":
                    pipeline_json = session.get_object(Bucket='pantavisor-ci', Key=obj['Key'])
                    pipe['metadata'] = json.loads(pipeline_json['Body'].read().decode())
                    pipe['metadata']['path'] = obj_list[0] + "/" + obj_list[1]
                elif file_name.endswith(".json"):
                    img_json = session.get_object(Bucket='pantavisor-ci', Key=obj['Key'])
                    img_metadata = json.loads(img_json['Body'].read().decode())
                    pipe['architectures'][pathlib.Path(file_name).stem] = img_metadata
    print("collect_pipelines_info:", len(pipelines), "pipelines retreived from objects")
    return pipelines

def get_latest_stable(pipelines):
    for pipe in sorted(pipelines.keys(), reverse=True):
        if pipelines[pipe]['metadata'] is not None and pipelines[pipe]['metadata']['gitdescribe'].isdigit():
            return pipe
    return {}

def populate_initial_images(pipelines):
    imgs_file = open('json/initial-images.json', 'r')
    imgs_json = json.load(imgs_file)

    stable_pipe = get_latest_stable(pipelines)
    print("populate_initial_images: populating stable with pipeline id", stable_pipe)

    stable_metadata = pipelines[stable_pipe]['metadata']
    stable_architectures = pipelines[stable_pipe]['architectures']

    # we moved from branch to ref in new builds
    key = 'ref'
    if not key in stable_metadata:
      key = 'branch'

    gitdescribe = stable_metadata[key]
    project = stable_metadata['project']
    committime = stable_metadata['committime']
    pvrversion = stable_metadata['pvrversion'].split(' ')[2]
    path = stable_metadata['path']

    imgs_json[5]['ul'].append("Release: %s - [release notes](https://gitlab.com/%s/-/releases/%s)" % (gitdescribe,project,gitdescribe))
    imgs_json[5]['ul'].append("Date: %s" % (committime))
    imgs_json[5]['ul'].append("BSP: %s - [release notes](https://gitlab.com/pantacor/pv-manifest/-/releases/%s)" % (gitdescribe,gitdescribe))
    imgs_json[5]['ul'].append("Pantavisor: %s - [release notes](https://github.com/pantavisor/pantavisor/releases/tag/%s)" % (gitdescribe,gitdescribe))
    imgs_json[5]['ul'].append("pvr version: [%s](https://gitlab.com/pantacor/pvr/-/tags/%s)" % (pvrversion,pvrversion))

    for arch in stable_architectures:
        arch_metadata = stable_architectures[arch]
        # old format did not have images array
        if not 'images' in arch_metadata:
            arch_row = []
            arch_row.append('**%s**' % (arch_metadata['platform']))
            arch_row.append('%s' % (arch_metadata['target']))
            arch_row.append('[download](https://pantavisor-ci.s3.amazonaws.com/%s/%s/%s/%s.img.xz)' % (path,gitdescribe,stable_pipe,arch))
            if 'aws_url_sha' in arch_metadata:
                arch_row.append('%s' % (arch_metadata['aws_url_sha']))
            imgs_json[7]['table']['rows'].append(arch_row)
            if 'installer' in arch_metadata and arch_metadata['installer'] is not None and arch_metadata['installer'] == "yes":
                arch_row = []
                arch_row.append('**%s**' % (arch_metadata['platform']))
                arch_row.append('%s-installer' % (arch_metadata['target']))
                arch_row.append('[download](https://pantavisor-ci.s3.amazonaws.com/%s/%s/%s/%s-installer.img.xz)' % (path,gitdescribe,stable_pipe,arch))
                if 'aws_installer_url_sha' in arch_metadata:
                    arch_row.append('%s' % (arch_metadata['aws_installer_url_sha']))
                imgs_json[7]['table']['rows'].append(arch_row)
        else:
            # new json format has images array - just show that flattened
            for i in arch_metadata['images']:
                arch_row = []
                arch_row.append('**%s**' % (arch_metadata['platform']))
                arch_row.append('%s (%s)' % (arch_metadata['target'], i['name']))
                if 'tezi_url' in i and i['tezi_url'] != "":
                    arch_row.append('[tezi download](%s)' % (i['tezi_url']))
                    if 'tezi_checksum' in i:
                        arch_row.append('%s' % (i['tezi_checksum']))
                    imgs_json[7]['table']['rows'].append(arch_row)
                elif 'rootfs_url' in i and i['rootfs_url'] != "":
                    arch_row.append('[rootfs download](%s)' % (i['rootfs_url']))
                    if 'rootfs_checksum' in i:
                        arch_row.append('%s' % (i['rootfs_checksum']))
                    imgs_json[7]['table']['rows'].append(arch_row)
                elif 'appengine_url' in i and i['appengine_url'] != "":
                    arch_row.append('[appengine download](%s)' % (i['appengine_url']))
                    if 'appengine_checksum' in i:
                        arch_row.append('%s' % (i['appengine_checksum']))
                    imgs_json[7]['table']['rows'].append(arch_row)
                else:
                    arch_row.append('[img download](%s)' % (i['url']))
                    if 'checksum' in i:
                        arch_row.append('%s' % (i['checksum']))
                    imgs_json[7]['table']['rows'].append(arch_row)

    print("populate_initial_images: populating pipeline table with", len(pipelines), "pipelines")

    tag_list = []

    for pipe in sorted(pipelines.keys(), reverse=True):
        print("populate_initial_images: processing pipeline", pipe)

        pipe_metadata = pipelines[pipe]['metadata']
        pipe_architectures = pipelines[pipe]['architectures']

        if pipe_metadata is not None:

            gitdescribe = ''
            if 'ref' in pipe_metadata:
                gitdescribe = pipe_metadata['ref']
            else:
                gitdescribe = pipe_metadata['branch']

            project = pipe_metadata['project']
            pipeline = pipe_metadata['pipeline']
            committime = pipe_metadata['committime']
            pvrversion = pipe_metadata['pvrversion']
            path = pipe_metadata['path']

            if gitdescribe not in tag_list:
                tag_list.append(gitdescribe)
                pipe_row = []
                pipe_row.append('[%s](https://gitlab.com/%s/-/tags/%s)' % (gitdescribe,project,gitdescribe))
                pipe_row.append('[%s](https://gitlab.com/%s/pipelines/%s)' % (pipeline,project,pipeline))
                pipe_row.append('%s' % (committime))
                pipe_row.append('%s' % (pvrversion.split(' ')[2]))

                images=''
                for arch in pipe_architectures:
                    arch_metadata = pipe_architectures[arch]
                    # format v0
                    if not 'images' in arch_metadata:
                        if 'target' in arch_metadata:
                            images+='[%s](https://pantavisor-ci.s3.amazonaws.com/%s/%s/%s/%s.img.xz) \| ' % (arch_metadata['target'],path,gitdescribe,pipe,arch)
                        if 'installer' in arch_metadata and arch_metadata['installer'] == "yes":
                            images+='[%s-installer](https://pantavisor-ci.s3.amazonaws.com/%s/%s/%s/%s-installer.img.xz) \| ' % (arch_metadata['target'],path,gitdescribe,pipe,arch)
                    else:
                        # format v1
                        for i in arch_metadata['images']:
                            if 'url' in i and i['url'] != "":
                                images+='[%s-%s](%s) \| ' % (arch_metadata['target'],i['name'],i['url'])
                            elif 'tezi_url' in i and i['tezi_url'] != "":
                                images+='[%s-%s](%s) \| ' % (arch_metadata['target'],i['name'],i['tezi_url'])
                            elif 'rootfs_url' in i and i['rootfs_url'] != "":
                                images+='[%s-%s](%s) \| ' % (arch_metadata['target'],i['name'],i['rootfs_url'])
                            elif 'appengine_url' in i and i['appengine_url'] != "":
                                images+='[%s-%s](%s) \| ' % (arch_metadata['target'],i['name'],i['appengine_url'])
                            else:
                                images+='UNKNOWN-image %s-%s \| ' % (arch_metadata['target'],i['name'])

                pipe_row.append(images[:-4])

                imgs_json[10]['table']['rows'].append(pipe_row)

    imgs_file.close()
    return imgs_json

def write_output_download_images(imgs_json):
    imgs_file = open('initial-images.json', 'w')
    imgs_file.write(json.dumps(imgs_json))
    imgs_file.close()

if len(sys.argv) == 3:
    s3 = authenticate(sys.argv[1], sys.argv[2])

    pipelines = {}
    pipelines.update(collect_pipelines_info(s3, 'pantavisor-ci', 'pv-initial-devices/tags'))

    imgs_json = populate_initial_images(pipelines)

    write_output_download_images(imgs_json)
