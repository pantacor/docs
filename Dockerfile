FROM alpine:3.20 as builder

WORKDIR /work

RUN apk update && \
	apk add \
	gcc \
	g++ \
	python3 \
	python3-dev \
	py3-regex \
	linux-headers \
	mkdocs \
	mkdocs-material \
	mkdocs-material-extensions

COPY . .
RUN mkdocs build

FROM nginx

COPY --from=builder /work/site /usr/share/nginx/html
